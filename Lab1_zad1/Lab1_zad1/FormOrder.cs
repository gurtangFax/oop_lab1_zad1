﻿using Podaci;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab1_zad1
{
    public partial class FormOrder : Form
    {
        #region Atributes
        Color DesignColor = Color.DarkKhaki;
        #endregion

        #region Constructors
        public FormOrder()
        {
            InitializeComponent();
            
        }

        #endregion

        #region Methodes
        private bool ValidacijaKontrola()
        {
            
            if (String.IsNullOrEmpty(txtCustomer.Text))
            {
                MessageBox.Show("Customer ne sme biti prazno polje.", "Obavestenje",
                                 MessageBoxButtons.OK, MessageBoxIcon.Information);

                return false;
            }

            if (String.IsNullOrEmpty(txtShipVia.Text))
            {
                MessageBox.Show("Ship Via ne sme biti prazno polje.", "Obavestenje",
                                 MessageBoxButtons.OK, MessageBoxIcon.Information);

                return false;
            }

            if (txtOrderID.Text.Length < 8)
            {
                MessageBox.Show("ID mora sadrzati 8 cifara.", "Obavestenje",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);

                return false;
            }

            return true;
            
        }

        #endregion

        #region EventHandlers

        #endregion
    }
}
