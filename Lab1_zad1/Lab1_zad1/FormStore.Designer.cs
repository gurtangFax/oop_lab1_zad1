﻿namespace Lab1_zad1
{
    partial class FormStore
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtOrderID = new System.Windows.Forms.TextBox();
            this.dtpPurchased1 = new System.Windows.Forms.DateTimePicker();
            this.comboStatus = new System.Windows.Forms.ComboBox();
            this.dtpPurchased2 = new System.Windows.Forms.DateTimePicker();
            this.dgvOrders = new System.Windows.Forms.DataGridView();
            this.ColumnOrder = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnPurchased = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnBill = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnShip = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnIncome = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnFilter = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnPending = new System.Windows.Forms.Button();
            this.btnProcessing = new System.Windows.Forms.Button();
            this.btnComplete = new System.Windows.Forms.Button();
            this.btnFile = new System.Windows.Forms.Button();
            this.btnExport = new System.Windows.Forms.Button();
            this.btnImport = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvOrders)).BeginInit();
            this.SuspendLayout();
            // 
            // txtOrderID
            // 
            this.txtOrderID.Location = new System.Drawing.Point(37, 9);
            this.txtOrderID.Name = "txtOrderID";
            this.txtOrderID.Size = new System.Drawing.Size(118, 20);
            this.txtOrderID.TabIndex = 0;
            // 
            // dtpPurchased1
            // 
            this.dtpPurchased1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpPurchased1.Location = new System.Drawing.Point(161, 8);
            this.dtpPurchased1.Name = "dtpPurchased1";
            this.dtpPurchased1.Size = new System.Drawing.Size(134, 20);
            this.dtpPurchased1.TabIndex = 1;
            // 
            // comboStatus
            // 
            this.comboStatus.FormattingEnabled = true;
            this.comboStatus.Location = new System.Drawing.Point(420, 8);
            this.comboStatus.Name = "comboStatus";
            this.comboStatus.Size = new System.Drawing.Size(109, 21);
            this.comboStatus.TabIndex = 2;
            // 
            // dtpPurchased2
            // 
            this.dtpPurchased2.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpPurchased2.Location = new System.Drawing.Point(301, 9);
            this.dtpPurchased2.Name = "dtpPurchased2";
            this.dtpPurchased2.Size = new System.Drawing.Size(113, 20);
            this.dtpPurchased2.TabIndex = 1;
            // 
            // dgvOrders
            // 
            this.dgvOrders.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvOrders.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnOrder,
            this.ColumnPurchased,
            this.ColumnBill,
            this.ColumnShip,
            this.ColumnIncome,
            this.ColumnStatus});
            this.dgvOrders.Location = new System.Drawing.Point(37, 35);
            this.dgvOrders.Name = "dgvOrders";
            this.dgvOrders.Size = new System.Drawing.Size(643, 293);
            this.dgvOrders.TabIndex = 3;
            // 
            // ColumnOrder
            // 
            this.ColumnOrder.HeaderText = "Order #";
            this.ColumnOrder.Name = "ColumnOrder";
            // 
            // ColumnPurchased
            // 
            this.ColumnPurchased.HeaderText = "Purchased On";
            this.ColumnPurchased.Name = "ColumnPurchased";
            // 
            // ColumnBill
            // 
            this.ColumnBill.HeaderText = "Bill to Name";
            this.ColumnBill.Name = "ColumnBill";
            // 
            // ColumnShip
            // 
            this.ColumnShip.HeaderText = "Ship to Name";
            this.ColumnShip.Name = "ColumnShip";
            // 
            // ColumnIncome
            // 
            this.ColumnIncome.HeaderText = "Income";
            this.ColumnIncome.Name = "ColumnIncome";
            // 
            // ColumnStatus
            // 
            this.ColumnStatus.HeaderText = "Status";
            this.ColumnStatus.Name = "ColumnStatus";
            // 
            // btnFilter
            // 
            this.btnFilter.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnFilter.ForeColor = System.Drawing.SystemColors.Control;
            this.btnFilter.Location = new System.Drawing.Point(535, 8);
            this.btnFilter.Name = "btnFilter";
            this.btnFilter.Size = new System.Drawing.Size(68, 20);
            this.btnFilter.TabIndex = 4;
            this.btnFilter.Text = "Filter";
            this.btnFilter.UseVisualStyleBackColor = false;
            this.btnFilter.Click += new System.EventHandler(this.BtnFilter_Click);
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(609, 8);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(101, 23);
            this.btnClear.TabIndex = 5;
            this.btnClear.Text = "Clear Filters";
            this.btnClear.UseVisualStyleBackColor = true;
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(37, 334);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 23);
            this.btnAdd.TabIndex = 6;
            this.btnAdd.Text = "ADD";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.Button2_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.Location = new System.Drawing.Point(118, 334);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(75, 23);
            this.btnEdit.TabIndex = 6;
            this.btnEdit.Text = "EDIT";
            this.btnEdit.UseVisualStyleBackColor = true;
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(199, 334);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 23);
            this.btnDelete.TabIndex = 6;
            this.btnDelete.Text = "DELETE";
            this.btnDelete.UseVisualStyleBackColor = true;
            // 
            // btnPending
            // 
            this.btnPending.Location = new System.Drawing.Point(346, 334);
            this.btnPending.Name = "btnPending";
            this.btnPending.Size = new System.Drawing.Size(85, 23);
            this.btnPending.TabIndex = 6;
            this.btnPending.Text = "->Pending";
            this.btnPending.UseVisualStyleBackColor = true;
            // 
            // btnProcessing
            // 
            this.btnProcessing.Location = new System.Drawing.Point(437, 334);
            this.btnProcessing.Name = "btnProcessing";
            this.btnProcessing.Size = new System.Drawing.Size(85, 23);
            this.btnProcessing.TabIndex = 6;
            this.btnProcessing.Text = "->Processing";
            this.btnProcessing.UseVisualStyleBackColor = true;
            // 
            // btnComplete
            // 
            this.btnComplete.Location = new System.Drawing.Point(528, 334);
            this.btnComplete.Name = "btnComplete";
            this.btnComplete.Size = new System.Drawing.Size(85, 23);
            this.btnComplete.TabIndex = 6;
            this.btnComplete.Text = "->Complete";
            this.btnComplete.UseVisualStyleBackColor = true;
            // 
            // btnFile
            // 
            this.btnFile.Location = new System.Drawing.Point(625, 334);
            this.btnFile.Name = "btnFile";
            this.btnFile.Size = new System.Drawing.Size(85, 23);
            this.btnFile.TabIndex = 6;
            this.btnFile.Text = "Export to FILE";
            this.btnFile.UseVisualStyleBackColor = true;
            // 
            // btnExport
            // 
            this.btnExport.Location = new System.Drawing.Point(50, 378);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(105, 23);
            this.btnExport.TabIndex = 6;
            this.btnExport.Text = "Export";
            this.btnExport.UseVisualStyleBackColor = true;
            this.btnExport.Click += new System.EventHandler(this.Button2_Click);
            // 
            // btnImport
            // 
            this.btnImport.Location = new System.Drawing.Point(161, 378);
            this.btnImport.Name = "btnImport";
            this.btnImport.Size = new System.Drawing.Size(105, 23);
            this.btnImport.TabIndex = 6;
            this.btnImport.Text = "Import";
            this.btnImport.UseVisualStyleBackColor = true;
            this.btnImport.Click += new System.EventHandler(this.Button2_Click);
            // 
            // FormStore
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnFile);
            this.Controls.Add(this.btnComplete);
            this.Controls.Add(this.btnProcessing);
            this.Controls.Add(this.btnPending);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnEdit);
            this.Controls.Add(this.btnImport);
            this.Controls.Add(this.btnExport);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnFilter);
            this.Controls.Add(this.dgvOrders);
            this.Controls.Add(this.comboStatus);
            this.Controls.Add(this.dtpPurchased2);
            this.Controls.Add(this.dtpPurchased1);
            this.Controls.Add(this.txtOrderID);
            this.Name = "FormStore";
            this.Text = "FormStore";
            ((System.ComponentModel.ISupportInitialize)(this.dgvOrders)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtOrderID;
        private System.Windows.Forms.DateTimePicker dtpPurchased1;
        private System.Windows.Forms.ComboBox comboStatus;
        private System.Windows.Forms.DateTimePicker dtpPurchased2;
        private System.Windows.Forms.DataGridView dgvOrders;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnOrder;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnPurchased;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnBill;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnShip;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnIncome;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnStatus;
        private System.Windows.Forms.Button btnFilter;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnPending;
        private System.Windows.Forms.Button btnProcessing;
        private System.Windows.Forms.Button btnComplete;
        private System.Windows.Forms.Button btnFile;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.Button btnImport;
    }
}