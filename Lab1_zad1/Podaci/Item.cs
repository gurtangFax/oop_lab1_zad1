﻿namespace Podaci
{
    public class Item
    {
        #region Atributes
        private string _name;
        private decimal _price;
        private int _quantity;
        #endregion

        #region Properties


        public int Quantity
        {
            get { return _quantity; }
            set { _quantity = value; }
        }

        public decimal UnitPrice
        {
            get { return _price; }
            set { _price = value; }
        }


        public string ProductName
        {
            get { return _name; }
            set { _name = value; }
        }

        #endregion


        #region Constructors
        public Item()
        {
                
        }
        public Item(string ime,decimal price,int quan)
        {
            _name = ime;
            _price = price;
            _quantity = quan;
        }

        #endregion





    }
}