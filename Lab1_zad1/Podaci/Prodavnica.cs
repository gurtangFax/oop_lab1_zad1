﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Podaci
{
    public class Prodavnica
    {

        #region Atributes
        public bool DodajNarudzbenicu(Narudzbenica n)
        {
            if (PostojiNarudzbenica(n))
                return false;

            _orders.Add(n);
            return true;
        }
        public bool ObrisiNarudzbenicu(Narudzbenica o)
        {
            if (!PostojiNarudzbenica(o))
                return false;

            _orders.Remove(o);
            return true;
        }

        public bool PostojiNarudzbenica(Narudzbenica o)
        {
            foreach (var v in _orders)
            {
                if (v.OrderID==o.OrderID)
                    return true;
            }

            return false;
        }
        public Narudzbenica GetNarudzbenica(int id)
        {
            foreach (var v in _orders)
            {
                if (v.OrderID == id)
                    return v;
            }

            return null;
        }

        #endregion

        #region Properties
        private List<Narudzbenica> _orders;

        public List<Narudzbenica> Orders
        {
            get { return _orders; }
            set { _orders = value; }
        }

        #endregion

        #region Constructors
        public Prodavnica()
        {
            _orders = new List<Narudzbenica>();
        }
        #endregion
        private static Prodavnica _instance = null;
        public static Prodavnica Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new Prodavnica();

                return _instance;
            }
        }
    }
}
