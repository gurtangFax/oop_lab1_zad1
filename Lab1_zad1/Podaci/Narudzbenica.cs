﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Podaci
{
    public class Narudzbenica
    {
        #region Atributes
        private DateTime _dateRequired;
        private DateTime _dateShipped;
        private DateTime _orderDate;
        private DateTime _datePurchased;
        private string _shipVia;
        private decimal _charges;
        private int _orderID;
        private Customer _customer;
        private List<Item> _items;
        private OrderStatus _status;
        private decimal _income;
        private string _ship;
        private string _bill;
        #endregion

        #region Methodes
        public bool DodajItem(Item n)
        {
            if (PostojiItem(n))
                return false;

            _items.Add(n);
            return true;
        }
        public bool PostojiItem(Item o)
        {
            foreach (var v in _items)
            {
                if ( v.ProductName ==o.ProductName)
                    return true;
            }

            return false;
        }
        public bool ObrisiItem(Item o)
        {
            if (!PostojiItem(o))
                return false;

            _items.Remove(o);
            return true;
        }
        public Item GetItem(string n)
        {
            foreach (var v in _items)
            {
                if (v.ProductName == n)
                    return v;
            }

            return null;
        }
        #endregion

        #region Properties


        public OrderStatus Status
        {
            get { return _status; }
            set { _status = value; }
        }

        public decimal Income
        {
            get { return _income; }
            set { _income = value; }
        }

        public string Ship
        {
            get { return _ship; }
            set { _ship = value; }
        }

        public string Bill
        {
            get { return _bill; }
            set { _bill = value; }
        }

        public DateTime DatePurchased
        {
            get { return _datePurchased; }
            set { _datePurchased = value; }
        }


        public List<Item> Items
        {
            get { return _items; }
            set { _items = value; }
        }

        public Customer Customer
        {
            get { return _customer; }
            set { _customer = value; }
        }

        public int OrderID
        {
            get { return _orderID; }
            set { _orderID = value; }
        }

        public decimal Charges
        {
            get { return _charges; }
            set { _charges = value; }
        }

        public DateTime OrderDate
        {
            get { return _orderDate; }
            set { _orderDate = value; }
        }


        public string ShipVia
        {
            get { return _shipVia; }
            set { _shipVia = value; }
        }

        public DateTime DateShipped
        {
            get { return _dateShipped; }
            set { _dateShipped = value; }
        }

        public DateTime DateRequired
        {
            get { return _dateRequired; }
            set { _dateRequired = value; }
        }

        #endregion

        #region Constructors
        public Narudzbenica()
        {
            _items = new List<Item>();
        }
        #endregion
    }

    public enum OrderStatus
    {
        Pending,
        Processing,
        Complete
    }
}

